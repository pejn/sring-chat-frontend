var ip = "http://192.168.1.36:8080";
var lastFetchTime = null;
var roomUuid = "";


function getAllRooms(){
    $(".rooms li:not(.template)").remove();

    $.get(ip + "/room", function (data) {
        $.each(data, function (key, item) {
            var room = $(".room.template").clone();

            console.log(item);
            room.removeClass("template");
            room.find("strong").html(item.name);
            room.attr("data-uuid", item.id);
            room.appendTo(".rooms ul");
        })

        $(".room").unbind("click").click(function () {
            var title = $(this).find("strong").html();
            var uuid = $(this).attr("data-uuid");
            // alert(uuid);
            roomUuid = uuid;
            console.log(title);
           $(".main-header").html(title);
        });
    });
}

function getAllMessages() {
    $.get(ip + "/message", function (data) {
        $.each(data, function (key, item) {
            var message = $(".message.template").clone();
            console.log(item)
            message.removeClass("template");
            message.find("span").html(moment(item.dateTime).format('HH:mm:ss'));
            message.find(".content").html(item.content);
            message.appendTo(".messages");
        });

        $(".messages").scrollTop(9999999);
    });

    lastFetchTime = Date.now();
}


function getNewMessagesSince() {
    $.get(ip + "/message/since/" + lastFetchTime, function (data) {
        $.each(data, function (key, item) {
            var message = $(".message.template").clone();
            console.log(item)
            message.removeClass("template");
            message.find("span").html(moment(item.dateTime).format('HH:mm:ss'));
            message.find(".content").html(item.content);
            message.appendTo(".messages");

        });
        $(".messages").scrollTop(9999999);
    });


    lastFetchTime = Date.now();
}


$(document).ready(function () {
    var count = 0;

    var input = $("input");

    getAllMessages(); 
    getAllRooms();
    setInterval(getNewMessagesSince, 500);



    input.keydown(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".btn").click();
        }
    });

    $("img").click(function () {
        var name = prompt("Podaj nazwę nowego pokoju!");
        if (name != "" && name != null) {
            $.ajax({
                url: ip + "/room",
                type: "POST",
                data: JSON.stringify({name: name}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                complete: function (response) {
                    if (response.responseJSON.error != undefined) {
                        alert(response.responseJSON.message)
                    } else {
                        getAllRooms();
                    }
                }
            });
        } else {
            alert("Pokój musi mieć nazwę !") };

    });




    $(".btn").click(function () {
        var message = $(".message.template").clone();
        var value = input.val();

        if (value == "") {
            input.animate({
                backgroundColor: "#FF0000"
            }, {
                duration: 500,
                complete: function () {
                    input.animate({
                        backgroundColor: "#FFF"
                    }, {
                        duration: 500,
                    });
                }
            });
        } else {
            $.ajax({
                url: ip + "/message",
                type: "POST",
                data: JSON.stringify({message: value}),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            input.val("");
        }
    });
});
